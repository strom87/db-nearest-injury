package db

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type SpotCommentReplyProvider struct {
	*Connection
	Collection string
}

func NewSpotCommentReplyProvider(connection *Connection) *SpotCommentReplyProvider {
	return &SpotCommentReplyProvider{Connection: connection, Collection: "spot_comment_replies"}
}

func (p SpotCommentReplyProvider) Find(id bson.ObjectId) (*CommentReply, error) {
	db := p.Connection.GetCollection(p.Collection)

	reply := &CommentReply{}
	if err := db.Find(bson.M{"_id": id}).One(reply); err != nil {
		return nil, err
	}

	return reply, nil
}

func (p SpotCommentReplyProvider) FindAll(id bson.ObjectId) ([]CommentReply, error) {
	db := p.Connection.GetCollection(p.Collection)

	replies := []CommentReply{}
	if err := db.Find(bson.M{"_id": id}).All(replies); err != nil {
		return nil, err
	}

	return replies, nil
}

func (p SpotCommentReplyProvider) FindByUser(id bson.ObjectId) (*CommentReply, error) {
	db := p.Connection.GetCollection(p.Collection)

	reply := &CommentReply{}
	if err := db.Find(bson.M{"user_id": id}).One(reply); err != nil {
		return nil, err
	}

	return reply, nil
}

func (p SpotCommentReplyProvider) Insert(reply CommentReply) error {
	return p.Connection.GetCollection(p.Collection).Insert(reply)
}

func (p SpotCommentReplyProvider) UpdateAllUserName(userId bson.ObjectId, name string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)

	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_name": name}})
}

func (p SpotCommentReplyProvider) UpdateAllUserImage(userId bson.ObjectId, image string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)

	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_image": image}})
}

func (p SpotCommentReplyProvider) RemoveAll() {
	p.Connection.GetCollection(p.Collection).RemoveAll(nil)
}
