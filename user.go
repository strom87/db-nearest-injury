package db

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id            bson.ObjectId        `json:"id"         bson:"_id,omitempty"`
	Name          string               `json:"name"       bson:"name"`
	Email         string               `json:"-"          bson:"email"`
	IsActivated   bool                 `json:"-"          bson:"is_activated"`
	Image         string               `json:"image"      bson:"image"`
	RegisterDate  time.Time            `json:"-"          bson:"register_date"`
	LastLoginDate time.Time            `json:"-"          bson:"last_login_date"`
	Credential    Credential           `json:"-"          bson:"credential"`
	TokenSecret   TokenSecretContainer `json:"-"          bson:"token_secret"`
}
