package db

import "gopkg.in/mgo.v2/bson"

type Like struct {
	Likes   int64           `json:"likes"    bson:"likes"`
	LikedBy []bson.ObjectId `json:"liked_by" bson:"liked_by"`
}
