package db

type TokenSecretContainer struct {
	Web TokenSecret `json:"-" bson:"web"`
	App TokenSecret `json:"-" bson:"app"`
}
