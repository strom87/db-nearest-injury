package db

import "gopkg.in/mgo.v2/bson"

type UserImageProvider struct {
	*Connection
	Collection string
}

func NewUserImageProvider(connection *Connection) *UserImageProvider {
	return &UserImageProvider{Connection: connection, Collection: "user_images"}
}

func (p UserImageProvider) Find(id bson.ObjectId) (*UserImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	image := &UserImage{}
	if err := db.Find(bson.M{"_id": id}).One(image); err != nil {
		return nil, err
	}

	return image, nil
}

func (p UserImageProvider) FindAll(id bson.ObjectId) ([]UserImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	images := []UserImage{}
	if err := db.Find(bson.M{"_id": id}).All(images); err != nil {
		return nil, err
	}

	return images, nil
}

func (p UserImageProvider) FindByUserId(id bson.ObjectId) (*UserImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	image := &UserImage{}
	if err := db.Find(bson.M{"user_id": id}).One(image); err != nil {
		return nil, err
	}

	return image, nil
}

func (p UserImageProvider) FindAllByUserId(id bson.ObjectId) ([]UserImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	var images []UserImage
	if err := db.Find(bson.M{"user_id": id}).All(&images); err != nil {
		return nil, err
	}

	return images, nil
}

func (p UserImageProvider) Insert(images []UserImage) error {
	for _, image := range images {
		if err := p.Connection.GetCollection(p.Collection).Insert(&image); err != nil {
			return err
		}
	}

	return nil
}
