package db

import (
	"strings"

	"gopkg.in/mgo.v2/bson"
)

type UserProvider struct {
	*Connection
	Collection string
}

func NewUserProvider(connection *Connection) *UserProvider {
	return &UserProvider{Connection: connection, Collection: "users"}
}

func (p UserProvider) Find(id bson.ObjectId) (*User, error) {
	db := p.Connection.GetCollection(p.Collection)

	user := &User{}
	if err := db.Find(bson.M{"_id": id}).One(user); err != nil {
		return nil, err
	}

	return user, nil
}

func (p UserProvider) FindByEmail(email string) (*User, error) {
	db := p.Connection.GetCollection(p.Collection)

	user := &User{}
	if err := db.Find(bson.M{"email": strings.ToLower(email)}).One(user); err != nil {
		return nil, err
	}

	return user, nil
}

func (p UserProvider) Insert(user User) error {
	return p.Connection.GetCollection(p.Collection).Insert(user)
}

func (p UserProvider) Update(user User) error {
	return p.Connection.GetCollection(p.Collection).Update(bson.M{"_id": user.Id}, user)
}

func (p UserProvider) Remove(id bson.ObjectId) error {
	return p.Connection.GetCollection(p.Collection).Remove(bson.M{"_id": id})
}

func (p UserProvider) RemoveAll() {
	p.Connection.GetCollection(p.Collection).RemoveAll(nil)
}

func (p UserProvider) SetImage(id bson.ObjectId, image string) error {
	db := p.Connection.GetCollection(p.Collection)

	return db.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"image": image}})
}

func (p UserProvider) Activate(id bson.ObjectId) error {
	db := p.Connection.GetCollection(p.Collection)

	return db.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"is_activated": true}})
}

func (p UserProvider) Exist(id bson.ObjectId) (bool, error) {
	db := p.Connection.GetCollection(p.Collection)

	if count, err := db.Find(bson.M{"_id": id}).Count(); err != nil {
		return false, err
	} else {
		return count != 0, nil
	}
}

func (p UserProvider) EmailExist(email string) (bool, error) {
	db := p.Connection.GetCollection(p.Collection)

	if count, err := db.Find(bson.M{"email": strings.ToLower(email)}).Count(); err != nil {
		return false, err
	} else {
		return count != 0, nil
	}
}

func (p UserProvider) NameExist(name string) (bool, error) {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"name": bson.M{
			"$regex": bson.RegEx{
				"^" + name + "$", "i",
			},
		},
	}

	if count, err := db.Find(query).Count(); err != nil {
		return false, err
	} else {
		return count != 0, nil
	}
}

func (p UserProvider) SetAuthTokenWebSecret(user User, secret string) error {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"$set": bson.M{
			"token_secret.web.auth": secret,
		},
	}

	return db.Update(bson.M{"_id": user.Id}, query)
}

func (p UserProvider) SetAuthTokenAppSecret(user User, secret string) error {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"$set": bson.M{
			"token_secret.app.auth": secret,
		},
	}

	return db.Update(bson.M{"_id": user.Id}, query)
}

func (p UserProvider) SetRefreshTokenWebSecret(user User, secret string) error {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"$set": bson.M{
			"token_secret.web.refresh": secret,
		},
	}

	return db.Update(bson.M{"_id": user.Id}, query)
}

func (p UserProvider) SetRefreshTokenAppSecret(user User, secret string) error {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"$set": bson.M{
			"token_secret.app.refresh": secret,
		},
	}

	return db.Update(bson.M{"_id": user.Id}, query)
}
