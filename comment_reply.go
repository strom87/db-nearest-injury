package db

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type CommentReply struct {
	Id        bson.ObjectId `json:"id"         bson:"_id,omitempty"`
	CommentId bson.ObjectId `json:"comment_id" bson:"comment_id"`
	UserId    bson.ObjectId `json:"user_id"    bson:"user_id"`
	UserName  string        `json:"user_name"  bson:"user_name"`
	UserImage string        `json:"user_image" bson:"user_image"`
	Message   string        `json:"message"    bson:"message"`
	Created   time.Time     `json:"created"    bson:"created"`
}
