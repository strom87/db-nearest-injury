package db

import "gopkg.in/mgo.v2/bson"

type Image struct {
	Id       bson.ObjectId `json:"id"        bson:"_id,omitempty"`
	UserId   bson.ObjectId `json:"user_id"   bson:"user_id,omitempty"`
	UserName string        `json:"user_name" bson:"user_name"`
	File     string        `json:"file"      bson:"file"`
	Like     Like          `json:"like"      bson:"like"`
}
