package db

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type SpotImageProvider struct {
	*Connection
	Collection string
}

func NewSpotImageProvider(connection *Connection) *SpotImageProvider {
	return &SpotImageProvider{Connection: connection, Collection: "spot_images"}
}

func (p SpotImageProvider) Find(id bson.ObjectId) (*SpotImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	image := &SpotImage{}
	if err := db.Find(bson.M{"_id": id}).One(image); err != nil {
		return nil, err
	}

	return image, nil
}

func (p SpotImageProvider) FindAll(id bson.ObjectId) ([]SpotImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	images := []SpotImage{}
	if err := db.Find(bson.M{"_id": id}).All(images); err != nil {
		return nil, err
	}

	return images, nil
}

func (p SpotImageProvider) FindBySpotId(id bson.ObjectId) (*SpotImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	image := &SpotImage{}
	if err := db.Find(bson.M{"spot_id": id}).One(image); err != nil {
		return nil, err
	}

	return image, nil
}

func (p SpotImageProvider) FindAllBySpotId(id bson.ObjectId) ([]SpotImage, error) {
	db := p.Connection.GetCollection(p.Collection)

	var images []SpotImage
	if err := db.Find(bson.M{"spot_id": id}).All(&images); err != nil {
		return nil, err
	}

	return images, nil
}

func (p SpotImageProvider) Insert(images []SpotImage) error {
	for _, image := range images {
		if err := p.Connection.GetCollection(p.Collection).Insert(&image); err != nil {
			return err
		}
	}

	return nil
}

func (p SpotImageProvider) LikeImage(id bson.ObjectId, like Like) error {
	db := p.Connection.GetCollection(p.Collection)

	return db.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"like": like}})
}

func (p SpotImageProvider) UpdateAllUserName(userId bson.ObjectId, name string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)
	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_name": name}})
}

func (p SpotImageProvider) UpdateAllUserImage(userId bson.ObjectId, image string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)

	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_image": image}})
}
