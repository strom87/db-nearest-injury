package db

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type SpotCommentProvider struct {
	*Connection
	Collection string
}

func NewSpotCommentProvider(connection *Connection) *SpotCommentProvider {
	return &SpotCommentProvider{Connection: connection, Collection: "spot_comments"}
}

func (p SpotCommentProvider) Find(id bson.ObjectId) (*Comment, error) {
	db := p.Connection.GetCollection(p.Collection)

	comment := &Comment{}
	if err := db.Find(bson.M{"_id": id}).One(comment); err != nil {
		return nil, err
	}

	return comment, nil
}

func (p SpotCommentProvider) FindBySpotId(id bson.ObjectId) ([]Comment, error) {
	db := p.Connection.GetCollection(p.Collection)

	var comments []Comment
	if err := db.Find(bson.M{"spot_id": id}).All(&comments); err != nil {
		return nil, err
	}

	return comments, nil
}

func (p SpotCommentProvider) Insert(comment Comment) error {
	return p.Connection.GetCollection(p.Collection).Insert(comment)
}

func (p SpotCommentProvider) UpdateAllUserName(userId bson.ObjectId, name string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)

	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_name": name}})
}

func (p SpotCommentProvider) UpdateAllUserImage(userId bson.ObjectId, image string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)

	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_image": image}})
}

func (p SpotCommentProvider) SetHasReplies(id bson.ObjectId, value bool) error {
	db := p.Connection.GetCollection(p.Collection)

	return db.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"has_replies": value}})
}

func (p SpotCommentProvider) RemoveAll() {
	p.Connection.GetCollection(p.Collection).RemoveAll(nil)
}
