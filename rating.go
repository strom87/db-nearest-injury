package db

import "gopkg.in/mgo.v2/bson"

type Rating struct {
	Score   float32          `json:"score" bson:"score"`
	Values  map[string]int64 `json:"-"     bson:"values"`
	RatedBy []bson.ObjectId  `json:"-"     bson:"rated_by"`
}
