package db

import "time"

type Credential struct {
	Salt       string    `json:"-" bson:"salt"`
	Password   string    `json:"-" bson:"password"`
	RehashDate time.Time `json:"-" bson:"rehash_date"`
}
