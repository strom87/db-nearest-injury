package db

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type SpotProvider struct {
	*Connection
	Collection string
}

func NewSpotProvider(connection *Connection) *SpotProvider {
	return &SpotProvider{Connection: connection, Collection: "spots"}
}

func (p SpotProvider) Find(id bson.ObjectId) (*Spot, error) {
	db := p.Connection.GetCollection(p.Collection)

	spot := &Spot{}
	if err := db.Find(bson.M{"_id": id}).One(spot); err != nil {
		return nil, err
	}

	return spot, nil
}

func (p SpotProvider) FindByGeoLocation(lat, long float64, distance int64) ([]Spot, error) {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"location": bson.M{
			"$near": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{lat, long},
				},
				"$maxDistance": distance,
			},
		},
	}

	var spots []Spot
	if err := db.Find(query).All(&spots); err != nil {
		return nil, err
	}

	return spots, nil
}

func (p SpotProvider) FindByGeoLocationAndUser(lat, long float64, distance int64, userId bson.ObjectId) ([]Spot, error) {
	db := p.Connection.GetCollection(p.Collection)

	query := bson.M{
		"location": bson.M{
			"$near": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{lat, long},
				},
				"$maxDistance": distance,
			},
		},
		"user_id": userId,
	}

	var spots []Spot
	if err := db.Find(query).All(&spots); err != nil {
		return nil, err
	}

	return spots, nil
}

func (p SpotProvider) Insert(spot Spot) error {
	return p.Connection.GetCollection(p.Collection).Insert(spot)
}

func (p SpotProvider) Update(spot Spot) error {
	return p.Connection.GetCollection(p.Collection).Update(bson.M{"_id": spot.Id}, spot)
}

func (p SpotProvider) UpdateAllUserName(userId bson.ObjectId, name string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)
	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_name": name}})
}

func (p SpotProvider) UpdateAllUserImage(userId bson.ObjectId, image string) (*mgo.ChangeInfo, error) {
	db := p.Connection.GetCollection(p.Collection)

	return db.UpdateAll(bson.M{"user_id": userId}, bson.M{"$set": bson.M{"user_image": image}})
}

func (p SpotProvider) SetRating(id bson.ObjectId, rating Rating) error {
	db := p.Connection.GetCollection(p.Collection)

	return db.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"rating": rating}})
}

func (p SpotProvider) PushImage(id bson.ObjectId, images []Image) error {
	db := p.Connection.GetCollection(p.Collection)

	return db.Update(bson.M{"_id": id}, bson.M{"$push": bson.M{"images": bson.M{"$each": images}}})
}

func (p SpotProvider) Remove(id bson.ObjectId) error {
	return p.Connection.GetCollection(p.Collection).Remove(bson.M{"_id": id})
}

func (p SpotProvider) RemoveAll() {
	p.Connection.GetCollection(p.Collection).RemoveAll(nil)
}

func (p SpotProvider) IndexLocationGeo() error {
	return p.Connection.GetCollection(p.Collection).EnsureIndex(mgo.Index{Key: []string{"$2dsphere:location"}})
}
