package db

type TokenSecret struct {
	Auth    string `json:"-" bson:"auth"`
	Refresh string `json:"-" bson:"refresh"`
}
