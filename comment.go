package db

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Comment struct {
	Id         bson.ObjectId `json:"id"          bson:"_id,omitempty"`
	SpotId     bson.ObjectId `json:"spot_id"     bson:"spot_id"`
	UserId     bson.ObjectId `json:"user_id"     bson:"user_id"`
	UserName   string        `json:"user_name"   bson:"user_name"`
	UserImage  string        `json:"user_image"  bson:"user_image"`
	Message    string        `json:"message"     bson:"message"`
	HasReplies bool          `json:"has_replies" bson:"has_replies"`
	Created    time.Time     `json:"created"     bson:"created"`
}
