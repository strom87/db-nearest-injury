package db

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type Spot struct {
	Id          bson.ObjectId `json:"id"          bson:"_id,omitempty"`
	UserId      bson.ObjectId `json:"user_id"     bson:"user_id"`
	UserName    string        `json:"user_name"   bson:"user_name"`
	UserImage   string        `json:"user_image"  bson:"user_image"`
	Name        string        `json:"name"        bson:"name"`
	Description string        `json:"description" bson:"description"`
	Tags        []string      `json:"tags"        bson:"tags"`
	Created     time.Time     `json:"created"     bson:"created"`
	Location    Location      `json:"location"    bson:"location"`
	Rating      Rating        `json:"rating"      bson:"rating"`
}
